//
//  PopularsVC.swift
//  TheMovieDB
//
//  Created by Mac Hostienda Movil on 5/23/19.
//  Copyright © 2019 MujicaM. All rights reserved.
//

import UIKit
import PKHUD
import SDWebImage

enum segmedTittle: String {
    case peliculas
    case series
}


class PopularsVC: UIViewController {    

    //MARK: MenuItems
    @IBOutlet weak var contentViewMenu: UIView!
    @IBOutlet weak var segmendControl: UISegmentedControl!
    
    @IBOutlet weak var tfBuscador: UITextField!
    @IBOutlet weak var tfCategoria: UITextField!
    
    @IBOutlet weak var tableView: UITableView!
    var segmendSelect = segmedTittle.peliculas
    @IBOutlet var backView: UIView!
    
    var picker = UIPickerView()
    var arrayGenres : [Genres]? = []
    var movie : [MovieDetails]? = []
    var serie : [resultSeries]? = []
    
    var page: Int = 1
    var maxPage: Int = 1
    var refresher: UIRefreshControl?
    var isLastPageReach: Bool = false
    
    var presenter : PopularPresenter?
    
    var generoFilter: Genres?
    var isfilterGenres = false
    var filteForGenresMovies: [MovieDetails]? = []
    var filteForGenresSeries: [resultSeries]? = []
    
    
    //local filter
    var searchActive : Bool = false
    var filteredMovie: [MovieDetails]? = []
    var filteredSerie: [resultSeries]? = []
    
    
    //search Online
    var presenterSearch : SearchPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(MoviesCell.nib, forCellReuseIdentifier: "MoviesCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        presenter = PopularPresenter(self)
        self.presenterSearch = SearchPresenter(self)
        arrayGenres = Storage.GenresMovies?.genres
        picker.delegate = self
        tfCategoria.inputView = picker
        setupPagin()
        tfBuscador.addTarget(self, action: #selector(self.textFieldDidChange(_:)),for: .editingChanged)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       self.reloadData()
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        guard let searchText  = textField.text else {
            print("error en textfield")
            return
        }
        
        switch  segmendSelect {
            
        case .peliculas:
            // filter tableViewData with textField.text
            filteredMovie = movie?.filter({ (tmp) -> Bool in
                
                let isMachingWorker: NSString = (tmp.title! as NSString)
                let range = isMachingWorker.lowercased.range(of: searchText, options: NSString.CompareOptions.caseInsensitive, range: nil,   locale: nil)
                return range != nil
            })
            
            if(filteredMovie?.count == 0){
                searchActive = false;
            } else {
                searchActive = true;
            }
            self.tableView.reloadData()
            
        case .series:
            
            filteredSerie = serie?.filter({ (tmp) -> Bool in
                let isMachingWorker: NSString = (tmp.name! as NSString)
                let range = isMachingWorker.lowercased.range(of: searchText, options: NSString.CompareOptions.caseInsensitive, range: nil,   locale: nil)
                return range != nil
            })
            
            if(filteredSerie?.count == 0){
                searchActive = false;
            } else {
                searchActive = true;
            }
            self.tableView.reloadData()
        }
        
    }
    
    
    func clearPicker() {
        tfCategoria.text = ""
        isfilterGenres = false
        isLastPageReach  = false
        picker.selectRow(0, inComponent: 0, animated: false)
        
    }
    
    func setupPagin(){
        refresher =  UIRefreshControl()
        if let refresher = refresher {
            if #available(iOS 10.0, *) {
                tableView.refreshControl = refresher
            } else {
                tableView.addSubview(refresher)
            }
            refresher.endRefreshing()
            refresher.tintColor = .white
            refresher.addTarget(self, action: #selector(refresh), for: .valueChanged)
        }
    }
    
    @objc func refresh() {
        page = 1
        isLastPageReach = false
        HUD.show(.rotatingImage(#imageLiteral(resourceName: "progress_circular")))
        tfBuscador.text = ""
        clearPicker()
        searchActive = false
       if tfCategoria.text != "" {
            
        }else{
            switch segmendSelect {
            case .peliculas:
                presenter?.getMovies(page: page)
            case .series:
                presenter?.getSeries(page: page)
            }
        }

        refresher?.endRefreshing()
    }
    
    @IBAction func buscar(_ sender: UIButton) {
        clearPicker()
        guard let texto = tfBuscador.text, !texto.isEmpty else {
            self.showMessage(message: "El campo se encuantra vacio", title: "Atencion", okOption: "OK")
            return
        }
        let textito = texto.replacingOccurrences(of: " ", with: "%20").stripped
        page = 1
        switch segmendSelect {
        case .peliculas:
            searchActive = false
            HUD.show(.rotatingImage(#imageLiteral(resourceName: "progress_circular")))
            self.presenterSearch?.searchMovie(query: textito, page: page)
           
        case .series:
            searchActive = false
            HUD.show(.rotatingImage(#imageLiteral(resourceName: "progress_circular")))
            presenterSearch?.searchSerie(query: textito, page: page)
        }
        
    }
    
    @IBAction func segmendValue(_ sender: UISegmentedControl) {
        tfCategoria.text = ""
        page = 1
        if sender.selectedSegmentIndex == 0  {
            self.movie?.removeAll()
            self.segmendSelect = .peliculas
            self.arrayGenres = Storage.GenresMovies?.genres
            picker.delegate = self
            self.reloadData()
        }else{
            self.serie?.removeAll()
            self.segmendSelect = .series
            self.arrayGenres = Storage.GenresTV?.genres
            picker.delegate = self
            self.reloadData()
        }
        
    }
    
    @IBAction func goTop(_ sender: UIButton) {
        let indexPath = IndexPath(row: 0, section: 0)
        self.tableView.scrollToRow(at: indexPath , at: .top, animated: true)
    }
    
    func reloadData () {
        tableView.reloadData()
        HUD.show(.rotatingImage(#imageLiteral(resourceName: "progress_circular")))
        switch segmendSelect {
        case .peliculas:
            presenter?.getMovies(page: page)
        case .series:
            presenter?.getSeries(page: page)
        }
    }    

}



extension PopularsVC: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.arrayGenres?.count ?? 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.arrayGenres?[row].name
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if let selectGenres = self.arrayGenres?[row] {
            self.tfCategoria.text = selectGenres.name
            self.generoFilter = selectGenres
            self.filterCategoria(genero: selectGenres)
            self.view.endEditing(true)
        }
        
    }
    
    func filterCategoria(genero: Genres){
        
        isfilterGenres = true
        self.filteForGenresSeries?.removeAll()
        self.filteForGenresMovies?.removeAll()
        tableView.reloadData()
        switch segmendSelect {
        case .peliculas:
            if let moviewDetails = movie {
                for temp in moviewDetails {
                    
                    if let tempoGenres = temp.genre_ids {
                        for idGenres in tempoGenres {
                            if idGenres == generoFilter?.id {
                                filteForGenresMovies?.append(temp)
                            }
                        }
                    }
                }
            }
            tableView.reloadData()
        case .series:
            if let seriesDetails = serie {
                for temp in seriesDetails {
                    
                    if let tempoGenres = temp.genre_ids {
                        for idGenres in tempoGenres {
                            if idGenres == generoFilter?.id {
                                filteForGenresSeries?.append(temp)
                            }
                        }
                    }
                }
            }
            tableView.reloadData()
        }
        
    }
    
    
}

extension PopularsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch segmendSelect {
            
        case .peliculas:
            if searchActive {
                guard let count = filteredMovie?.count, count > 0 else {
                    self.tableView.backgroundView = backView
                    return 0
                }
                self.tableView.backgroundView = nil
                return count
            }else if isfilterGenres {
                if let count = filteForGenresMovies?.count, count > 0 {
                    self.tableView.backgroundView = nil
                    return count
                }else{
                    self.tableView.backgroundView = backView
                    return 0
                }
            }else{
                if let count = movie?.count, count > 0{
                    self.tableView.backgroundView = nil
                    return count
                }else{
                    self.tableView.backgroundView = backView
                    return 0
                }
            }
            
        case .series:
            if searchActive {
                guard let count = filteredSerie?.count, count > 0 else {
                    self.tableView.backgroundView = backView
                    return 0
                }
                self.tableView.backgroundView = nil
                return count
            }else if isfilterGenres {
                if let count = filteForGenresSeries?.count, count > 0 {
                    self.tableView.backgroundView = nil
                    return count
                }else{
                    self.tableView.backgroundView = backView
                    return 0
                }
            }else{
                if let count = serie?.count, count > 0 {
                    self.tableView.backgroundView = nil
                    return count
                }else{
                    self.tableView.backgroundView = backView
                    return 0
                }
            }
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch segmendSelect {
            
        case .peliculas:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MoviesCell") as! MoviesCell
           
            if searchActive {
                if let moviewDetails = filteredMovie?[indexPath.row] {
                    cell.setupView(movie: moviewDetails)
                }
                return cell
            }else if isfilterGenres {
                if let moviewDetails = filteForGenresMovies?[indexPath.row] {
                    cell.setupView(movie: moviewDetails)
                }
                return cell
                
            }else{
                if let moviewDetails = movie?[indexPath.row] {
                    cell.setupView(movie: moviewDetails)
                }
                return cell
            }
            
            
            
        case .series:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MoviesCell") as! MoviesCell
            if searchActive {
                if let serieDetails = filteredSerie?[indexPath.row] {
                    cell.setupViewSeries(serie: serieDetails)
                }
                return cell
            }else if isfilterGenres {
                if let serieDetails = filteForGenresSeries?[indexPath.row] {
                    cell.setupViewSeries(serie: serieDetails)
                }
                return cell
            }else{
                if let serieDetails = serie?[indexPath.row] {
                    cell.setupViewSeries(serie: serieDetails)
                }
                return cell
            }
            
        }
        

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc: AllDetailsVC = AllDetailsVC()
        
        switch segmendSelect {
            
        case .peliculas:
            vc.segmendSelect = .peliculas
            vc.movie = movie?[indexPath.row]
            
            navigationController?.pushViewController(vc, animated: true)
        case .series:
            vc.segmendSelect = .series
            vc.serie = serie?[indexPath.row]
            
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let index = indexPath.row
        switch segmendSelect {
            
        case .peliculas:
            
            if isfilterGenres {
                if let countCarray = filteForGenresMovies?.count {
                    if index == (countCarray - 1) && !isLastPageReach && page <= maxPage {
                        HUD.show(.rotatingImage(#imageLiteral(resourceName: "progress_circular")))
                        //tableView.reloadRows(at: [indexPath], with: .none)
                        page +=  1
                        presenter?.getMovies(page: page)
                        
                    }
                }
            }else{
                if let countCarray = movie?.count {
                    if index == (countCarray - 1) && !isLastPageReach && page <= maxPage {
                        HUD.show(.rotatingImage(#imageLiteral(resourceName: "progress_circular")))
                        //tableView.reloadRows(at: [indexPath], with: .none)
                        page +=  1
                        presenter?.getMovies(page: page)
                        
                    }
                }
            }
            
        case .series:
            
            if isfilterGenres {
                if let countCarray = filteForGenresSeries?.count {
                    if index == (countCarray - 1) && !isLastPageReach && page <= maxPage {
                        HUD.show(.rotatingImage(#imageLiteral(resourceName: "progress_circular")))
                        //tableView.reloadRows(at: [indexPath], with: .none)
                        page +=  1
                        presenter?.getSeries(page: page)
                    }
                }
            }else{
                if let countCarray = serie?.count {
                    if index == (countCarray - 1) && !isLastPageReach && page <= maxPage {
                        HUD.show(.rotatingImage(#imageLiteral(resourceName: "progress_circular")))
                        //tableView.reloadRows(at: [indexPath], with: .none)
                        page +=  1
                        presenter?.getSeries(page: page)
                    }
                }
            }
            
        }
        
    }
    
}

extension PopularsVC: popularDelegate {
    
    func success(movie: [MovieDetails], totalPage: Int) {
        HUD.hide()
        if page == 1 {
            self.movie = movie
        }else{
            if let array1 = self.movie  {
                let flattenCollection = [array1, movie].joined()
                let tempArray = Array(flattenCollection)
                self.movie = tempArray
            }
        }
         isLastPageReach = false
        if movie.count == 0 {
            isLastPageReach = true
        }
        maxPage = totalPage
        
        refresher?.endRefreshing()
        if isfilterGenres {
            self.filterCategoria(genero: self.generoFilter!)
        }else{
            self.tableView.reloadData()
        }
    }
    
    func successSeries(series: [resultSeries], totalPage: Int) {
        HUD.hide()
        if page == 1 {
            self.serie = series
        }else{
            if let array1 = self.serie  {
                let flattenCollection = [array1, series].joined()
                let tempArray = Array(flattenCollection)
                self.serie = tempArray
            }
        }
        isLastPageReach = false
        if series.count == 0 {
            isLastPageReach = true
        }
        maxPage = totalPage
        
        refresher?.endRefreshing()
        if isfilterGenres {
            self.filterCategoria(genero: self.generoFilter!)
        }else{
            self.tableView.reloadData()
        }
    }
    
    func diderror(type: segmedTittle) {
        HUD.hide()
        showMessage(message: "Se mostraran los datos cargados previamente", title: "Sin Conexion", okOption: "OK")
        switch type {
        case .peliculas:
            self.movie = Storage.PopularMovies?.results
            isLastPageReach = true
            self.tableView.reloadData()
            break
        case .series:
            self.serie = Storage.PopularSeries?.results
            isLastPageReach = true
            self.tableView.reloadData()
            break
        }
    }
    
    
}


extension PopularsVC: searchDelegate {
    
    func searcMovieSucc(movie: [MovieDetails], totalPage: Int) {
        HUD.hide()
        if page == 1 {
            self.movie = movie
        }else{
            if let array1 = self.movie  {
                let flattenCollection = [array1, movie].joined()
                let tempArray = Array(flattenCollection)
                self.movie = tempArray
            }
        }
        isLastPageReach = false
        if movie.count == 0 {
            isLastPageReach = true
        }
        maxPage = totalPage
        
        refresher?.endRefreshing()
        if isfilterGenres {
            self.filterCategoria(genero: self.generoFilter!)
        }else{
            self.tableView.reloadData()
        }
    }
    
    func searchSerieSucc(series: [resultSeries], totalPage: Int) {
        HUD.hide()
        if page == 1 {
            self.serie = series
        }else{
            if let array1 = self.serie  {
                let flattenCollection = [array1, series].joined()
                let tempArray = Array(flattenCollection)
                self.serie = tempArray
            }
        }
        isLastPageReach = false
        if series.count == 0 {
            isLastPageReach = true
        }
        maxPage = totalPage
        
        refresher?.endRefreshing()
        if isfilterGenres {
            self.filterCategoria(genero: self.generoFilter!)
        }else{
            self.tableView.reloadData()
        }
    }
    
    func searchDiderror(type: segmedTittle) {
        HUD.hide()
        showMessage(message: "Se mostraran los datos cargados previamente", title: "Sin Conexion", okOption: "OK")
        switch type {
        case .peliculas:
            self.movie = Storage.PopularMovies?.results
            isLastPageReach = true
            self.tableView.reloadData()
            break
        case .series:
            self.serie = Storage.PopularSeries?.results
            isLastPageReach = true
            self.tableView.reloadData()
            break
        }
    }
    
    
}

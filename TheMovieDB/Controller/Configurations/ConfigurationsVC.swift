//
//  ConfigurationsVC.swift
//  TheMovieDB
//
//  Created by MacMujicaM on 5/27/19.
//  Copyright © 2019 MujicaM. All rights reserved.
//

import UIKit

class ConfigurationsVC: UIViewController {

    var presenter: ConfigurationPresenter?
    var window: UIWindow?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter = ConfigurationPresenter(delegate: self)
        self.presenter?.getMoviesGenres()
        // Do any additional setup after loading the view.
    }
    


}

extension ConfigurationsVC: configurationDelegate {
    
    func success() {
       
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewController = storyboard.instantiateViewController(withIdentifier: "MainNav")
        self.window?.rootViewController = initialViewController
        self.window?.makeKeyAndVisible()
    }
    
    func didErrors(error: String) {
        showMessage(message: "error: \(error)", title: "ERROR", okOption: "OK")
    }
    
    
}

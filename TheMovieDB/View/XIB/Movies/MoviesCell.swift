//
//  MoviesCell.swift
//  TheMovieDB
//
//  Created by Mac Hostienda Movil on 5/27/19.
//  Copyright © 2019 MujicaM. All rights reserved.
//

import UIKit
import Cosmos
import SDWebImage

class MoviesCell: UITableViewCell {
    
    @IBOutlet weak var imgMovies: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var ratingStarts: CosmosView!
    @IBOutlet weak var activityLoading: UIActivityIndicatorView!
    
    static let nib = UINib(nibName: "MoviesCell", bundle: nil)
    var movie : MovieModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func prepareForReuse() {
        
    }
    
    func setupView(movie: MovieDetails){
        activityLoading.startAnimating()
        self.lblName.text = movie.title
        self.ratingStarts.rating = movie.vote_average ?? 1.0
        
        if let path = movie.backdrop_path {
            let url = getImagesUrl(imageString: path)
            self.imgMovies?.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "logo"), options: SDWebImageOptions(rawValue: 0), completed: {
               (image, error, cacheType, imageURL) in
                self.imgMovies.image = image
                self.activityLoading.stopAnimating()
            })
        }else{
            if let path = movie.poster_path {
                let url = getImagesUrl(imageString: path)
                self.imgMovies?.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "logo"), options: SDWebImageOptions(rawValue: 0), completed: {
                    (image, error, cacheType, imageURL) in
                    self.imgMovies.image = image
                    self.activityLoading.stopAnimating()
                })
            }else{
                activityLoading.stopAnimating()
                self.imgMovies.image = #imageLiteral(resourceName: "logo")
            }
        }
    }
    
    func setupViewSeries(serie: resultSeries){
        activityLoading.startAnimating()
        self.lblName.text = serie.name
        self.ratingStarts.rating = serie.vote_average ?? 1.0
        
        if let path = serie.backdrop_path {
            let url = getImagesUrl(imageString: path)
            self.imgMovies?.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "logo"), options: SDWebImageOptions(rawValue: 0), completed: {
                (image, error, cacheType, imageURL) in
                self.imgMovies.image = image
                self.activityLoading.stopAnimating()
            })
        }else{
            if let path = serie.poster_path {
                let url = getImagesUrl(imageString: path)
                self.imgMovies?.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "logo"), options: SDWebImageOptions(rawValue: 0), completed: {
                    (image, error, cacheType, imageURL) in
                    self.imgMovies.image = image
                    self.activityLoading.stopAnimating()
                })
            }else{
                activityLoading.stopAnimating()
                self.imgMovies.image = #imageLiteral(resourceName: "logo")
            }
        }
    }
    
}

//
//  AllDetailsVC.swift
//  TheMovieDB
//
//  Created by Mac Hostienda Movil on 5/29/19.
//  Copyright © 2019 MujicaM. All rights reserved.
//

import UIKit
import PKHUD
import SDWebImage
import Cosmos
import WebKit

class AllDetailsVC: UIViewController {
    
    @IBOutlet weak var imgBackG: UIImageView!
    @IBOutlet weak var activitBackG: UIActivityIndicatorView!
    
    @IBOutlet weak var lblTiitle: UILabel!
//    @IBOutlet weak var webView: WKWebView!
//
//    @IBOutlet weak var activityLoadinWebV: UIActivityIndicatorView!
    
    @IBOutlet weak var imgPoster: UIImageView!
    @IBOutlet weak var activityLoadingPoster: UIActivityIndicatorView!
    
    @IBOutlet weak var lblPopularidad: UILabel!
    @IBOutlet weak var lblLenguaje: UILabel!
    @IBOutlet weak var textResumen: UITextView!
    
    @IBOutlet weak var ratingV: CosmosView!
    @IBOutlet weak var lblTotalVotos: UILabel!
    @IBOutlet weak var collectionVideos: UICollectionView!
    
    var segmendSelect = segmedTittle.peliculas
    var movie : MovieDetails?
    var serie : resultSeries?
    
    var presenter : VideosPresenter?
    var videoArrayKey: [VideosResults]? = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.presenter = VideosPresenter(self)
        self.collectionVideos.register(VideosCell.nib, forCellWithReuseIdentifier: "VideosCell")
        //self.webView.navigationDelegate = self
        self.setupView()
    }
    
    func setupView(){
        switch segmendSelect {
            
        case .peliculas:
            
            guard   let titulo = movie?.title,
                    let popular = movie?.popularity,
                    let lenguaje = movie?.original_language,
                    let resumen = movie?.overview,
                    let rating = movie?.vote_average,
                    let totalVotos = movie?.vote_count,
                    let fondo = movie?.backdrop_path,
                    let poster = movie?.poster_path, let id = movie?.id else{
                
                return
            }
            lblTiitle.text = titulo
            lblPopularidad.text = "Popularidad: \(popular)"
            lblLenguaje.text = "Lenguaje original: \(lenguaje)"
            textResumen.text = resumen
            ratingV.rating = rating
            lblTotalVotos.text = "Votos: \(totalVotos)"
            
            
            let url = getImagesUrl(imageString: fondo)
            self.imgBackG?.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "logo"), options: SDWebImageOptions(rawValue: 0), completed: {
                (image, error, cacheType, imageURL) in
                self.imgBackG.image = image
                self.activitBackG.stopAnimating()
            })
            
            let url2 = getImagesUrl(imageString: poster)
            self.imgPoster?.sd_setImage(with: URL(string: url2), placeholderImage: #imageLiteral(resourceName: "logo"), options: SDWebImageOptions(rawValue: 0), completed: {
                (image, error, cacheType, imageURL) in
                self.imgPoster.image = image
                self.activityLoadingPoster.stopAnimating()
            })
            HUD.show(.rotatingImage(#imageLiteral(resourceName: "progress_circular")))
            self.presenter?.getMovieVideo(id: id)
        case .series:
            guard   let titulo = serie?.name,
                    let popular = serie?.popularity,
                    let lenguaje = serie?.original_language,
                    let resumen = serie?.overview,
                    let rating = serie?.vote_average,
                    let totalVotos = serie?.vote_count,
                    let fondo = serie?.backdrop_path,
                    let poster = serie?.poster_path, let id = serie?.id else{
                    
                    return
            }
            lblTiitle.text = titulo
            lblPopularidad.text = "Popularidad: \(popular)"
            lblLenguaje.text = "Lenguaje original: \(lenguaje)"
            textResumen.text = resumen
            ratingV.rating = rating
            lblTotalVotos.text = "Votos: \(totalVotos)"
            
            let url = getImagesUrlOriginal(imageString: fondo)
            self.imgBackG?.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "logo"), options: SDWebImageOptions(rawValue: 0), completed: {
                (image, error, cacheType, imageURL) in
                self.imgBackG.image = image
                self.activitBackG.stopAnimating()
            })
            
            let url2 = getImagesUrl(imageString: poster)
            self.imgPoster?.sd_setImage(with: URL(string: url2), placeholderImage: #imageLiteral(resourceName: "logo"), options: SDWebImageOptions(rawValue: 0), completed: {
                (image, error, cacheType, imageURL) in
                self.imgPoster.image = image
                self.activityLoadingPoster.stopAnimating()
            })
            HUD.show(.rotatingImage(#imageLiteral(resourceName: "progress_circular")))
            self.presenter?.getTVVideo(id: id)
        }
    }

    @IBAction func goBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    

}

extension AllDetailsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if let count = self.videoArrayKey?.count, count > 0 {
            
            return count
        }else{
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VideosCell", for: indexPath) as! VideosCell
        cell.setupVideo(videoID: self.videoArrayKey?[indexPath.row].key ?? "YgSW4fnmlKs")
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width - 2
        let height = collectionView.bounds.height - 8
        return CGSize(width: width, height: height)
    }
    
    
}

extension AllDetailsVC: videosDelegate {
    
    
    func getMovieYoutubeID(videos: [VideosResults]) {
        HUD.hide()
        self.videoArrayKey = videos
        collectionVideos.reloadData()
    }
    
    func getTVYoutubeID(videos: [VideosResults]) {
        HUD.hide()
        self.videoArrayKey = videos
        collectionVideos.reloadData()
    }
    
    func didError(error: String) {
        HUD.hide()
        let videoID = "YgSW4fnmlKs"
        let map = VideosResults(JSON: [
            "id": "5bce4de892514105770067b4",
            "iso_639_1": "en",
            "iso_3166_1": "US",
            "key": videoID,
            "name": "One Punch Man - Official Trailer",
            "site": "YouTube",
            "size": 1080,
            "type": "Trailer"
            ])
        
        let map2 = [map]
        self.videoArrayKey = map2 as? [VideosResults]
        collectionVideos.reloadData()

    }
    
    
}

extension AllDetailsVC: WKNavigationDelegate {
//    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
//        self.activityLoadinWebV.stopAnimating()
//    }
}

//
//  VideosCell.swift
//  TheMovieDB
//
//  Created by Mac Hostienda Movil on 5/30/19.
//  Copyright © 2019 MujicaM. All rights reserved.
//

import UIKit
import WebKit

class VideosCell: UICollectionViewCell {

    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var activityLoadinWebV: UIActivityIndicatorView!
    static let nib = UINib(nibName: "VideosCell", bundle: nil)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    func setupVideo (videoID: String){
        self.webView.navigationDelegate = self
        let myUrl = URL(string: "https://www.youtube.com/embed/\(videoID)?enablejsapi=1&rel=0&playsinline=1&autoplay=0")
        self.webView.configuration.allowsAirPlayForMediaPlayback = true
        self.webView.scrollView.isScrollEnabled = false
        if let myUrl = myUrl {
            let consult = URLRequest(url: myUrl)
            self.webView.load(consult)
        }
    }

}

extension VideosCell: WKNavigationDelegate{
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.activityLoadinWebV.stopAnimating()
    }
    
}

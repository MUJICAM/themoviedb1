//
//  Storage.swift
//  TheMovieDB
//
//  Created by MacMujicaM on 5/27/19.
//  Copyright © 2019 MujicaM. All rights reserved.
//

import Foundation
import ObjectMapper

class Storage {
    
    fileprivate static let tvGenres = "TV_GENRES"
    fileprivate static let moviesGenres = "MOVIE_GENRES"
    fileprivate static let movies = "MOVIES"
    fileprivate static let series = "SERIES"
    
    //Generos
    class var GenresTV : GenresModel? {
        get {
            if let userString = UserDefaults.standard.string(forKey: tvGenres) {
                return Mapper<GenresModel>().map(JSONString: userString)
            }
            return  nil
        }
        set {
            UserDefaults.standard.set(newValue?.toJSONString(), forKey: tvGenres)
        }
    }
    
    class var GenresMovies : GenresModel? {
        get {
            if let userString = UserDefaults.standard.string(forKey: moviesGenres) {
                return Mapper<GenresModel>().map(JSONString: userString)
            }
            return  nil
        }
        set {
            UserDefaults.standard.set(newValue?.toJSONString(), forKey: moviesGenres)
        }
    }
    //Fin Generos
    
    //Popular
    class var PopularMovies : MovieModel? {
        get {
            if let userString = UserDefaults.standard.string(forKey: movies) {
                return Mapper<MovieModel>().map(JSONString: userString)
            }
            return  nil
        }
        set {
            UserDefaults.standard.set(newValue?.toJSONString(), forKey: movies)
        }
    }
    
    class var PopularSeries : SeriesModel? {
        get {
            if let userString = UserDefaults.standard.string(forKey: series) {
                return Mapper<SeriesModel>().map(JSONString: userString)
            }
            return  nil
        }
        set {
            UserDefaults.standard.set(newValue?.toJSONString(), forKey: series)
        }
    }
    
    
    //TopRated
    class var TopRatedMovies : MovieModel? {
        get {
            if let userString = UserDefaults.standard.string(forKey: movies) {
                return Mapper<MovieModel>().map(JSONString: userString)
            }
            return  nil
        }
        set {
            UserDefaults.standard.set(newValue?.toJSONString(), forKey: movies)
        }
    }
    
    class var TopRatedSeries : SeriesModel? {
        get {
            if let userString = UserDefaults.standard.string(forKey: series) {
                return Mapper<SeriesModel>().map(JSONString: userString)
            }
            return  nil
        }
        set {
            UserDefaults.standard.set(newValue?.toJSONString(), forKey: series)
        }
    }
    
    //Upcoming
    class var UpcomingMovies : MovieModel? {
        get {
            if let userString = UserDefaults.standard.string(forKey: movies) {
                return Mapper<MovieModel>().map(JSONString: userString)
            }
            return  nil
        }
        set {
            UserDefaults.standard.set(newValue?.toJSONString(), forKey: movies)
        }
    }
    
    class var UpcomingSeries : SeriesModel? {
        get {
            if let userString = UserDefaults.standard.string(forKey: series) {
                return Mapper<SeriesModel>().map(JSONString: userString)
            }
            return  nil
        }
        set {
            UserDefaults.standard.set(newValue?.toJSONString(), forKey: series)
        }
    }
    
    
    class func removeStorage() {
//        UserDefaults.standard.removeObject(forKey: userKey)
//        UserDefaults.standard.removeObject(forKey: lastUserLoggedInKey)
    }
}

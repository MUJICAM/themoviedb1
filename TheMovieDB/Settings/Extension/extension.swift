//
//  extension.swift
//  TheMovieDB
//
//  Created by Mac Hostienda Movil on 5/27/19.
//  Copyright © 2019 MujicaM. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func showMessage(message:String, title:String, okOption:String, completion: (()->Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: okOption, style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: completion)
    }
    
    func showMessage(message:String, title:String, with actions:[UIAlertAction]) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for a in actions {
            alert.addAction(a)
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    func showSheet(with actions:[UIAlertAction], completion: (()->Void)? = nil) {
        let sheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        for a in actions {
            sheet.addAction(a)
        }
        self.present(sheet, animated: true, completion: completion)
    }
    
}

extension String {
    
    var stripped: String {
        return self.folding(options: .diacriticInsensitive, locale: .current)
    }
    
}

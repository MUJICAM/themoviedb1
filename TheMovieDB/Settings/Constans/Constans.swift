//
//  Constans.swift
//  TheMovieDB
//
//  Created by Mac Hostienda Movil on 5/27/19.
//  Copyright © 2019 MujicaM. All rights reserved.
//

import Foundation
var apiKey = "9476fdb3db3e872243ad80d91f6cac46"
var localLanguage = "es" //Locale.current.languageCode ?? "es"

//Generos url
let urlGetTVList = "https://api.themoviedb.org/3/genre/tv/list?api_key=\(apiKey)&language=\(localLanguage)"
let urlGetMovieList = "https://api.themoviedb.org/3/genre/movie/list?api_key=\(apiKey)&language=\(localLanguage)"

//movies url
func getPopularMovies(page: Int) -> String {return "https://api.themoviedb.org/3/movie/popular?api_key=\(apiKey)&language=\(localLanguage)&page=\(page)"}
func getTopRatedMovies (page: Int) -> String { return "https://api.themoviedb.org/3/movie/top_rated?api_key=\(apiKey)&language=\(localLanguage)&page=\(page)"}
func getUpcomingMovies (page: Int) -> String{return "https://api.themoviedb.org/3/movie/upcoming?api_key=\(apiKey)&language=\(localLanguage)&page=\(page)"}

func getDetailsMovies(id: String) -> String {return "https://api.themoviedb.org/3/movie/\(id)?api_key=\(apiKey)&language=\(localLanguage)"}

//Series
func getPopularSeries (page: Int) -> String {return "https://api.themoviedb.org/3/tv/popular?api_key=\(apiKey)&language=\(localLanguage)&page=\(page)"}
func getTopRatedSeries (page: Int) -> String {return "https://api.themoviedb.org/3/tv/top_rated?api_key=\(apiKey)&language=\(localLanguage)&page=\(page)"}
func getUpcomingSeries (page: Int) -> String {return "https://api.themoviedb.org/3/tv/on_the_air?api_key=\(apiKey)&language=\(localLanguage)&page=\(page)"}

func getDetailsSeries(id: String) -> String {return "https://api.themoviedb.org/3/tv/\(id)?api_key=\(apiKey)&language=\(localLanguage)"}

//multimedia
func getImagesUrl(imageString :String) -> String {return "https://image.tmdb.org/t/p/w500\(imageString)"}
func getImagesUrlOriginal(imageString :String) -> String {return "https://image.tmdb.org/t/p/original\(imageString)"}
func getvdeosUrlTV(id: Int) -> String {return "https://api.themoviedb.org/3/tv/\(id)/videos?api_key=\(apiKey)"}
func getvideosUrlMovies(id: Int) -> String {return "https://api.themoviedb.org/3/movie/\(id)/videos?api_key=\(apiKey)&language=\(localLanguage)"}

//multiSearch
func movieSearchUrl(query: String) -> String {return "https://api.themoviedb.org/3/search/movie?api_key=\(apiKey)&language=\(localLanguage)&query=\(query)&page=1&include_adult=false"}
func serieSearchUrl(query: String) -> String {return "https://api.themoviedb.org/3/search/tv?api_key=\(apiKey)&language=\(localLanguage)&query=\(query)&page=1&include_adult=false"}
func multiSearchUrl(query: String) -> String {return "https://api.themoviedb.org/3/search/multi?api_key=\(apiKey)&language=\(localLanguage)&query=\(query)&page=1&include_adult=false"}

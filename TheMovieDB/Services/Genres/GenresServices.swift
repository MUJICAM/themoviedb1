//
//  GenresServices.swift
//  TheMovieDB
//
//  Created by MacMujicaM on 5/27/19.
//  Copyright © 2019 MujicaM. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

class GenresServices {
    
    class func  getMoviesGenres(successBlock:@escaping ( _ onSuccess: String) -> (), errorBlock: @escaping (_ error: String) -> ()){
        
        Alamofire.request(urlGetMovieList, method: .get, parameters: nil, headers: nil).responseObject{
             (responses : DataResponse<GenresModel>) in
            
            switch responses.result{
                
            case .success(_):
                if let result = responses.result.value {
                    Storage.GenresMovies = result
                    successBlock("Succes")
                }else{
                    errorBlock("Error en parseo")
                }
                
            case .failure(_):
                errorBlock("revisar conexion a internet")
                
            }
            
            
        }
    
    }
    
    class func getSeriesGenres(successBlock:@escaping ( _ onSuccess: String) -> (), errorBlock: @escaping (_ error: String) -> ()){
    
        Alamofire.request(urlGetTVList, method: .get, parameters: nil, headers: nil).responseObject{
            (responses : DataResponse<GenresModel>) in
            
            switch responses.result{
                
            case .success(_):
                if let result = responses.result.value {
                    Storage.GenresTV = result
                    successBlock("Succes")
                }else{
                    errorBlock("Error en parseo")
                }
                
            case .failure(_):
                errorBlock("revisar conexion a internet")
                
            }
            
            
        }
    }
}

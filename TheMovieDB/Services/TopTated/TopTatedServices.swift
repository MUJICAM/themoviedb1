//
//  TopTatedServices.swift
//  TheMovieDB
//
//  Created by Mac Hostienda Movil on 5/30/19.
//  Copyright © 2019 MujicaM. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper


class TopTatedServices {
    class func getTopTatedMovie(numberPage: Int, successBlock:@escaping ( _ onSuccess: MovieModel) -> (), errorBlock: @escaping (_ error: String) -> ()){
        let url = getTopRatedMovies(page: numberPage)
        Alamofire.request(url, method: .get, parameters: nil, headers: nil).responseObject{
            (response: DataResponse<MovieModel>) in
            switch response.result{
                
            case .success(_):
                if let result = response.result.value {
                    Storage.TopRatedMovies = result
                    successBlock(result)
                }else{
                    errorBlock("error")
                }
                break
            case .failure(_):
                errorBlock("error")
                break
            }
        }
        
    }
    
    class func getTopTatedSerie(numberPage: Int, successBlock:@escaping ( _ onSuccess: SeriesModel) -> (), errorBlock: @escaping (_ error: String) -> ()){
        let url = getTopRatedSeries(page: numberPage)
        Alamofire.request(url, method: .get, parameters: nil, headers: nil).responseObject{
            (response: DataResponse<SeriesModel>) in
            switch response.result{
                
            case .success(_):
                if let result = response.result.value {
                    Storage.TopRatedSeries = result
                    successBlock(result)
                }else{
                    errorBlock("error")
                }
                break
            case .failure(_):
                errorBlock("error")
                break
            }
        }
    }
}

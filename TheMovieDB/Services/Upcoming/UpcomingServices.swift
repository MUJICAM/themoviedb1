//
//  UpcomingServices.swift
//  TheMovieDB
//
//  Created by Mac Hostienda Movil on 5/30/19.
//  Copyright © 2019 MujicaM. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper


class UpcomingServices {
    class func getUpcomingMovie(numberPage: Int, successBlock:@escaping ( _ onSuccess: MovieModel) -> (), errorBlock: @escaping (_ error: String) -> ()){
        let url = getUpcomingMovies(page: numberPage)
        Alamofire.request(url, method: .get, parameters: nil, headers: nil).responseObject{
            (response: DataResponse<MovieModel>) in
            switch response.result{
                
            case .success(_):
                if let result = response.result.value {
                    Storage.UpcomingMovies = result
                    successBlock(result)
                }else{
                    errorBlock("error")
                }
                break
            case .failure(_):
                errorBlock("error")
                break
            }
        }
        
    }
    
    class func getUpcomingSerie(numberPage: Int, successBlock:@escaping ( _ onSuccess: SeriesModel) -> (), errorBlock: @escaping (_ error: String) -> ()){
        let url = getUpcomingSeries(page: numberPage)
        Alamofire.request(url, method: .get, parameters: nil, headers: nil).responseObject{
            (response: DataResponse<SeriesModel>) in
            switch response.result{
                
            case .success(_):
                if let result = response.result.value {
                    Storage.UpcomingSeries = result
                    successBlock(result)
                }else{
                    errorBlock("error")
                }
                break
            case .failure(_):
                errorBlock("error")
                break
            }
        }
    }
}

//
//  VideosServices.swift
//  TheMovieDB
//
//  Created by Mac Hostienda Movil on 5/29/19.
//  Copyright © 2019 MujicaM. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper


class VideosServices {
    
    class func getVideosMovie(id: Int, successBlock:@escaping ( _ onSuccess: VideosModel) -> (), errorBlock: @escaping (_ error: String) -> ()){
        let url = getvideosUrlMovies(id: id)
        Alamofire.request(url, method: .get, parameters: nil, headers: nil).responseObject{
            (response: DataResponse<VideosModel>) in
            switch response.result {
                
            case .success(_):
                if let result = response.result.value {
                    successBlock(result)
                }else{
                    errorBlock("errorPase")
                }
                
                
            case .failure(_):
                errorBlock("errorConection")
                
            }
        }
        
    }
    
    class func getVideosSerie(id: Int, successBlock:@escaping ( _ onSuccess: VideosModel) -> (), errorBlock: @escaping (_ error: String) -> ()){
         let url = getvdeosUrlTV(id: id)
        
        Alamofire.request(url, method: .get, parameters: nil, headers: nil).responseObject{
            (response: DataResponse<VideosModel>) in
            switch response.result {
                
            case .success(_):
                if let result = response.result.value {
                    successBlock(result)
                }else{
                    errorBlock("errorPase")
                }
                
                
            case .failure(_):
                errorBlock("errorConection")
                
            }
        }
        
        
    }
    
    
}

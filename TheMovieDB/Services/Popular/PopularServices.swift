//
//  PopularServices.swift
//  TheMovieDB
//
//  Created by MacMujicaM on 5/28/19.
//  Copyright © 2019 MujicaM. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

class PopularServices {
    
    class func getPopularMovie(numberPage: Int, successBlock:@escaping ( _ onSuccess: MovieModel) -> (), errorBlock: @escaping (_ error: String) -> ()){
        let url = getPopularMovies(page: numberPage)
        Alamofire.request(url, method: .get, parameters: nil, headers: nil).responseObject{
            (response: DataResponse<MovieModel>) in
            switch response.result{
                
            case .success(_):
                if let result = response.result.value {
                    Storage.PopularMovies = result
                     successBlock(result)
                }else{
                    errorBlock("error")
                }
                break
            case .failure(_):
                errorBlock("error")
                break
            }
        }
        
    }
    
    class func getPopularSerie(numberPage: Int, successBlock:@escaping ( _ onSuccess: SeriesModel) -> (), errorBlock: @escaping (_ error: String) -> ()){
        let url = getPopularSeries(page: numberPage)
        Alamofire.request(url, method: .get, parameters: nil, headers: nil).responseObject{
            (response: DataResponse<SeriesModel>) in
            switch response.result{
                
            case .success(_):
                if let result = response.result.value {
                    Storage.PopularSeries = result
                    successBlock(result)
                }else{
                    errorBlock("error")
                }
                break
            case .failure(_):
                errorBlock("error")
                break
            }
        }
    }
}

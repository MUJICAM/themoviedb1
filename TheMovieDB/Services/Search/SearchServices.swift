//
//  SearchServices.swift
//  TheMovieDB
//
//  Created by MacMujicaM on 5/28/19.
//  Copyright © 2019 MujicaM. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper


class SearhServices {
    class func getMultisearch(query: String, numberPage: Int, successBlock:@escaping ( _ onSuccess: MultiSearchModel) -> (), errorBlock: @escaping (_ error: String) -> ()){
        let url = multiSearchUrl(query: query)
        Alamofire.request(url, method: .get, parameters: nil, headers: nil).responseObject{
            (response: DataResponse<MultiSearchModel>) in
            
            switch response.result{
                
            case .success(_):
                if let result = response.result.value {
                    //Storage.Movies = result
                    successBlock(result)
                }else{
                    errorBlock("error")
                }
                break
            case .failure(_):
                errorBlock("error")
                break
            }
        }
    }
    
    class func getMoviewSearch(query: String, numberPage: Int, successBlock:@escaping ( _ onSuccess: MovieModel) -> (), errorBlock: @escaping (_ error: String) -> ()){
        let url = movieSearchUrl(query: query)
        Alamofire.request(url, method: .get, parameters: nil, headers: nil).responseObject{
            (response: DataResponse<MovieModel>) in
            
            switch response.result{
                
            case .success(_):
                if let result = response.result.value {
                    //Storage.Movies = result
                    successBlock(result)
                }else{
                    errorBlock("error")
                }
                break
            case .failure(_):
                errorBlock("error")
                break
            }
        }
    }
    
    class func getSerieSearch(query: String, numberPage: Int, successBlock:@escaping ( _ onSuccess: SeriesModel) -> (), errorBlock: @escaping (_ error: String) -> ()){
        let url = serieSearchUrl(query: query)
        Alamofire.request(url, method: .get, parameters: nil, headers: nil).responseObject{
            (response: DataResponse<SeriesModel>) in
            
            
            switch response.result{
                
            case .success(_):
                if let result = response.result.value {
                    //Storage.Series = result
                    successBlock(result)
                }else{
                    errorBlock("error")
                }
                break
            case .failure(_):
                errorBlock("error")
                break
            }
        }
    }
}

//
//  MoviesVideosModel.swift
//  TheMovieDB
//
//  Created by Mac Hostienda Movil on 5/27/19.
//  Copyright © 2019 MujicaM. All rights reserved.
//

import Foundation
import ObjectMapper

class VideosResults: NSObject, Mappable {
    var id : String?
    var iso_639_1 : String?
    var iso_3166_1 : String?
    var key : String?
    var name : String?
    var site : String?
    var size : Int?
    var type : String?
    
    convenience required init?(map: Map) {
        self.init()
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        iso_639_1 <- map["iso_639_1"]
        iso_3166_1 <- map["iso_3166_1"]
        key <- map["key"]
        name <- map["name"]
        site <- map["site"]
        size <- map["size"]
        type <- map["type"]
    }
}

class VideosModel: NSObject, Mappable {
    var id : Int?
    var results : [VideosResults]?
    
    convenience required init?(map: Map) {
         self.init()
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        results <- map["results"]
    }
    
}

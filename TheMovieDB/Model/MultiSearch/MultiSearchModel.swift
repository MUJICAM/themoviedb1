//
//  MultiSearchModel.swift
//  TheMovieDB
//
//  Created by Mac Hostienda Movil on 5/27/19.
//  Copyright © 2019 MujicaM. All rights reserved.
//

import Foundation
import ObjectMapper

class ResultMultiSearch: NSObject, Mappable {
    var original_name : String?
    var id : Int?
    var media_type : String?
    var name : String?
    var vote_count : Int?
    var vote_average : Double?
    var poster_path : String?
    var first_air_date : String?
    var popularity : Double?
    var genre_ids : [Int]?
    var original_language : String?
    var backdrop_path : String?
    var overview : String?
    var origin_country : [String]?
    
    convenience required init?(map: Map) {
        self.init()
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        
        original_name <- map["original_name"]
        id <- map["id"]
        media_type <- map["media_type"]
        name <- map["name"]
        vote_count <- map["vote_count"]
        vote_average <- map["vote_average"]
        poster_path <- map["poster_path"]
        first_air_date <- map["first_air_date"]
        popularity <- map["popularity"]
        genre_ids <- map["genre_ids"]
        original_language <- map["original_language"]
        backdrop_path <- map["backdrop_path"]
        overview <- map["overview"]
        origin_country <- map["origin_country"]
    }
}

class MultiSearchModel: NSObject, Mappable {
    var page : Int?
    var total_results : Int?
    var total_pages : Int?
    var results : [ResultMultiSearch]?
    
    convenience required init?(map: Map) {
        self.init()
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        
        page <- map["page"]
        total_results <- map["total_results"]
        total_pages <- map["total_pages"]
        results <- map["results"]
    }
}

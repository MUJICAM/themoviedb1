//
//  MoviesDetails.swift
//  TheMovieDB
//
//  Created by Mac Hostienda Movil on 5/27/19.
//  Copyright © 2019 MujicaM. All rights reserved.
//

import Foundation
import ObjectMapper

class Spoken_languages: NSObject, Mappable {
    var iso_639_1 : String?
    var name : String?
    
    
    convenience required init?(map: Map) {
        self.init()
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        
        iso_639_1 <- map["iso_639_1"]
        name <- map["name"]
    }
}

class Production_countries: NSObject, Mappable {
    var iso_3166_1 : String?
    var name : String?

    
    convenience required init?(map: Map) {
        self.init()
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        
        iso_3166_1 <- map["iso_3166_1"]
        name <- map["name"]
    }
}

class MoviesDetails: NSObject, Mappable {
    var adult : Bool?
    var backdrop_path : String?
    var belongs_to_collection : String?
    var budget : Int?
    var genres : [Genres]?
    var homepage : String?
    var id : Int?
    var imdb_id : String?
    var original_language : String?
    var original_title : String?
    var overview : String?
    var popularity : Double?
    var poster_path : String?
    var production_companies : [Production_companies]?
    var production_countries : [Production_countries]?
    var release_date : String?
    var revenue : Int?
    var runtime : Int?
    var spoken_languages : [Spoken_languages]?
    var status : String?
    var tagline : String?
    var title : String?
    var video : Bool?
    var vote_average : Double?
    var vote_count : Int?
    
    convenience required init?(map: Map) {
        self.init()
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        
    }
}

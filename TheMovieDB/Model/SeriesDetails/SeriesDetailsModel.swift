//
//  SeriesDetailsModel.swift
//  TheMovieDB
//
//  Created by Mac Hostienda Movil on 5/27/19.
//  Copyright © 2019 MujicaM. All rights reserved.
//

import Foundation
import ObjectMapper

class Seasons: NSObject, Mappable {
    var air_date : String?
    var episode_count : Int?
    var id : Int?
    var name : String?
    var overview : String?
    var poster_path : String?
    var season_number : Int?
    
    convenience required init?(map: Map) {
        self.init()
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        
        air_date <- map["air_date"]
        episode_count <- map["episode_count"]
        id <- map["id"]
        name <- map["name"]
        overview <- map["overview"]
        poster_path <- map["poster_path"]
        season_number <- map["season_number"]
    }
}
class Production_companies: NSObject, Mappable {
    var id : Int?
    var logo_path : String?
    var name : String?
    var origin_country : String?
    
    convenience required init?(map: Map) {
        self.init()
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        logo_path <- map["logo_path"]
        name <- map["name"]
        origin_country <- map["origin_country"]
    }
}

class Networks: NSObject, Mappable {
    var name : String?
    var id : Int?
    var logo_path : String?
    var origin_country : String?
    
    convenience required init?(map: Map) {
        self.init()
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        id <- map["id"]
        logo_path <- map["logo_path"]
        origin_country <- map["origin_country"]
    }
}

class Next_episode_to_air: NSObject, Mappable {
    var air_date : String?
    var episode_number : Int?
    var id : Int?
    var name : String?
    var overview : String?
    var production_code : String?
    var season_number : Int?
    var show_id : Int?
    var still_path : String?
    var vote_average : Int?
    var vote_count : Int?
    
    convenience required init?(map: Map) {
        self.init()
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        
        air_date <- map["air_date"]
        episode_number <- map["episode_number"]
        id <- map["id"]
        name <- map["name"]
        overview <- map["overview"]
        production_code <- map["production_code"]
        season_number <- map["season_number"]
        show_id <- map["show_id"]
        still_path <- map["still_path"]
        vote_average <- map["vote_average"]
        vote_count <- map["vote_count"]
    }
}

class Last_episode_to_air: NSObject, Mappable {
    var air_date : String?
    var episode_number : Int?
    var id : Int?
    var name : String?
    var overview : String?
    var production_code : String?
    var season_number : Int?
    var show_id : Int?
    var still_path : String?
    var vote_average : Int?
    var vote_count : Int?
    
    convenience required init?(map: Map) {
        self.init()
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        
        air_date <- map["air_date"]
        episode_number <- map["episode_number"]
        id <- map["id"]
        name <- map["name"]
        overview <- map["overview"]
        production_code <- map["production_code"]
        season_number <- map["season_number"]
        show_id <- map["show_id"]
        still_path <- map["still_path"]
        vote_average <- map["vote_average"]
        vote_count <- map["vote_count"]
    }
}

class Created_by: NSObject, Mappable {
    var id : Int?
    var credit_id : String?
    var name : String?
    var gender : Int?
    var profile_path : String?
    
    convenience required init?(map: Map) {
        self.init()
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        credit_id <- map["credit_id"]
        name <- map["name"]
        gender <- map["gender"]
        profile_path <- map["profile_path"]
    }
}

class SeriesDetailsModel: NSObject, Mappable {
    
    var backdrop_path : String?
    var created_by : [Created_by]?
    var episode_run_time : [Int]?
    var first_air_date : String?
    var genres : [Genres]?
    var homepage : String?
    var id : Int?
    var in_production : Bool?
    var languages : [String]?
    var last_air_date : String?
    var last_episode_to_air : Last_episode_to_air?
    var name : String?
    var next_episode_to_air : Next_episode_to_air?
    var networks : [Networks]?
    var number_of_episodes : Int?
    var number_of_seasons : Int?
    var origin_country : [String]?
    var original_language : String?
    var original_name : String?
    var overview : String?
    var popularity : Double?
    var poster_path : String?
    var production_companies : [Production_companies]?
    var seasons : [Seasons]?
    var status : String?
    var type : String?
    var vote_average : Double?
    var vote_count : Int?
   
    convenience required init?(map: Map) {
        self.init()
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        
        backdrop_path <- map["backdrop_path"]
        created_by <- map["created_by"]
        episode_run_time <- map["episode_run_time"]
        first_air_date <- map["first_air_date"]
        genres <- map["genres"]
        homepage <- map["homepage"]
        id <- map["id"]
        in_production <- map["in_production"]
        languages <- map["languages"]
        last_air_date <- map["last_air_date"]
        last_episode_to_air <- map["last_episode_to_air"]
        name <- map["name"]
        next_episode_to_air <- map["next_episode_to_air"]
        networks <- map["networks"]
        number_of_episodes <- map["number_of_episodes"]
        number_of_seasons <- map["number_of_seasons"]
        origin_country <- map["origin_country"]
        original_language <- map["original_language"]
        original_name <- map["original_name"]
        overview <- map["overview"]
        popularity <- map["popularity"]
        poster_path <- map["poster_path"]
        production_companies <- map["production_companies"]
        seasons <- map["seasons"]
        status <- map["status"]
        type <- map["type"]
        vote_average <- map["vote_average"]
        vote_count <- map["vote_count"]
    }
}

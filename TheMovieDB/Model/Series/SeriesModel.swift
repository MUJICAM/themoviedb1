//
//  SeriesModel.swift
//  TheMovieDB
//
//  Created by Mac Hostienda Movil on 5/27/19.
//  Copyright © 2019 MujicaM. All rights reserved.
//

import Foundation
import ObjectMapper

class resultSeries: NSObject, Mappable {
    var original_name : String?
    var genre_ids : [Int]?
    var name : String?
    var popularity : Double?
    var origin_country : [String]?
    var vote_count : Int?
    var first_air_date : String?
    var backdrop_path : String?
    var original_language : String?
    var id : Int?
    var vote_average : Double?
    var overview : String?
    var poster_path : String?
    
    convenience required init?(map: Map) {
        self.init()
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        
        original_name <- map["original_name"]
        genre_ids <- map["genre_ids"]
        name <- map["name"]
        popularity <- map["popularity"]
        origin_country <- map["origin_country"]
        vote_count <- map["vote_count"]
        first_air_date <- map["first_air_date"]
        backdrop_path <- map["backdrop_path"]
        original_language <- map["original_language"]
        id <- map["id"]
        vote_average <- map["vote_average"]
        overview <- map["overview"]
        poster_path <- map["poster_path"]
    }
}

class SeriesModel: NSObject, Mappable {
    
    var page : Int?
    var total_results : Int?
    var total_pages : Int?
    var results : [resultSeries]?
    
    convenience required init?(map: Map) {
        self.init()
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        
        page <- map["page"]
        total_results <- map["total_results"]
        total_pages <- map["total_pages"]
        results <- map["results"]
    }
}

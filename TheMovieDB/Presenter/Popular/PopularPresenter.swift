//
//  PopularPresenter.swift
//  TheMovieDB
//
//  Created by MacMujicaM on 5/28/19.
//  Copyright © 2019 MujicaM. All rights reserved.
//

import Foundation

protocol popularDelegate: class {
    func success(movie: [MovieDetails], totalPage: Int)
    func successSeries(series: [resultSeries], totalPage: Int)
    func diderror(type: segmedTittle)
}


class PopularPresenter {
    
    weak var delegate: popularDelegate?
   
    init(_ delegate: popularDelegate) {
        self.delegate = delegate
    }
    
    func getMovies(page: Int){
        PopularServices.getPopularMovie(numberPage: page, successBlock: {
            result in
            if let movies = result.results, let totalPage = result.total_pages {
                self.delegate?.success(movie: movies, totalPage: totalPage)
            }
        }, errorBlock: {
            error in
            self.delegate?.diderror(type: .peliculas)
        })
    }
    
    func getSeries(page: Int){
        PopularServices.getPopularSerie(numberPage: page, successBlock: {
            result in
            if let series = result.results, let totalPage = result.total_pages {
                self.delegate?.successSeries(series: series, totalPage: totalPage)
            }
        }, errorBlock: {
            error in
            self.delegate?.diderror(type: .series)
        })
    }
    
}

//
//  UpcomingPresenter.swift
//  TheMovieDB
//
//  Created by Mac Hostienda Movil on 5/30/19.
//  Copyright © 2019 MujicaM. All rights reserved.
//

import Foundation

protocol upcomingDelegate: class {
    func success(movie: [MovieDetails], totalPage: Int)
    func successSeries(series: [resultSeries], totalPage: Int)
    func diderror(type: segmedTittle)
}

class UpcomingPresenter  {
    
    weak var delegate: upcomingDelegate?
    
    init(_ delegate: upcomingDelegate) {
        self.delegate = delegate
    }
    
    func getMovies(page: Int){
        UpcomingServices.getUpcomingMovie(numberPage: page, successBlock: {
            result in
            if let movies = result.results, let totalPage = result.total_pages {
                self.delegate?.success(movie: movies, totalPage: totalPage)
            }
        }, errorBlock: {
            error in
            self.delegate?.diderror(type: .peliculas)
        })
    }
    
    func getSeries(page: Int){
        UpcomingServices.getUpcomingSerie(numberPage: page, successBlock: {
            result in
            if let series = result.results, let totalPage = result.total_pages {
                self.delegate?.successSeries(series: series, totalPage: totalPage)
            }
        }, errorBlock: {
            error in
            self.delegate?.diderror(type: .series)
        })
    }
}

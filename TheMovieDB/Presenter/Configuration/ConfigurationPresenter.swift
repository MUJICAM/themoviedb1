//
//  ConfigurationPresenter.swift
//  TheMovieDB
//
//  Created by MacMujicaM on 5/28/19.
//  Copyright © 2019 MujicaM. All rights reserved.
//

import Foundation
import UIKit

protocol configurationDelegate: class {
    func success()
    func didErrors(error: String)
}

class ConfigurationPresenter {
    weak var delegate : configurationDelegate?
    
    init(delegate: configurationDelegate) {
        self.delegate = delegate
    }
    
    func getMoviesGenres(){
        GenresServices.getMoviesGenres(successBlock: {
            result in
            self.getSeriesGenres()
        }, errorBlock: {
            error in
            print(error)
        })
    }
    
    func getSeriesGenres(){
        GenresServices.getSeriesGenres(successBlock: {
            result in
            self.delegate?.success()
        }, errorBlock: {
            error in
            print(error)
        })
    }
}

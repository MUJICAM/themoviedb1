//
//  VideosPresenter.swift
//  TheMovieDB
//
//  Created by Mac Hostienda Movil on 5/29/19.
//  Copyright © 2019 MujicaM. All rights reserved.
//

import Foundation

protocol videosDelegate: class {
    func getMovieYoutubeID(videos: [VideosResults])
    func getTVYoutubeID(videos: [VideosResults])
    func didError(error: String)
    
}

class VideosPresenter{
    weak var delegate : videosDelegate?
    
    init(_ delegate: videosDelegate) {
        self.delegate = delegate
        
    }
    
    func getMovieVideo(id: Int){
        VideosServices.getVideosMovie(id: id, successBlock: {
            result in
            if let tempo = result.results {
                self.delegate?.getMovieYoutubeID(videos: tempo)
            }
            
        }, errorBlock: {
            error in
            self.delegate?.didError(error: "error")
        })
    }
    
    func getTVVideo(id: Int){
        VideosServices.getVideosSerie(id: id, successBlock: {
            result in
            if let tempo = result.results {
                self.delegate?.getTVYoutubeID(videos: tempo)
            }
        }, errorBlock: {
            error in
            self.delegate?.didError(error: "error")
        })
    }
}

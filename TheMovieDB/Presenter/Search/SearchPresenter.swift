//
//  SearchPresenter.swift
//  TheMovieDB
//
//  Created by Mac Hostienda Movil on 5/28/19.
//  Copyright © 2019 MujicaM. All rights reserved.
//

import Foundation

protocol searchDelegate: class {
    func searcMovieSucc(movie: [MovieDetails], totalPage: Int)
    func searchSerieSucc(series: [resultSeries], totalPage: Int)
    func searchDiderror(type: segmedTittle)
}

class SearchPresenter {
    weak var delegate : searchDelegate?
    init(_ delegate: searchDelegate) {
        self.delegate = delegate
    }
    
    func searchMovie(query: String, page: Int){
        SearhServices.getMoviewSearch(query: query, numberPage: page, successBlock: {
            result in
            if let movie = result.results, let totalPage = result.total_pages {
                self.delegate?.searcMovieSucc(movie: movie, totalPage: totalPage)
            }
            
        }, errorBlock: {
            error in
            self.delegate?.searchDiderror(type: .peliculas)
        })
    }
    
    func searchSerie(query: String, page: Int){
        SearhServices.getSerieSearch(query: query, numberPage: page, successBlock: {
            result in
            if let serie = result.results, let totalPage = result.total_pages {
                self.delegate?.searchSerieSucc(series: serie, totalPage: totalPage)
            }
        }, errorBlock: {
            error in
            self.delegate?.searchDiderror(type: .series)
        })
    }
}


  capas de la applicacion trabajada 
    Modelo -> Contiene todos los modelos utilizados, separados por carpetas. contienen las siguientes clases.
      MoviesVideosModel.swift
      MoviesDetails.swift
      SeriesDetailsModel.swift
      SeriesModel.swift
      MultiSearchModel.swift
      GenresModel.swift
      MovieModel.swift

    View -> Contiene la interfas de la applicions, como los storyboard, Xib.
      XIB contiene lo siguiente:
        Details:
          VideosCell.swift y VideosCell.xib
          AllDetailsVC.swift y AllDetailsVC.xib
        Movies:
          MoviesCell.swift y MoviesCell.xib

    controllers -> Aqui tenemos el controlador de las vistas, en el cual se encuentra la logica y tratamiento de la vista
          Configurations/ConfigurationsVC.swift
          Upcoming/UpcomingVC.swift
          TopRated/TopRatedVC.swift
          Popular/PopularsVC.swift

    services -> Aqui tenemos separados por capetas el consumo de los diferentes servicios de la api que la aplicación utilizara
          Upcoming/UpcomingServices.swift
          TopTated/TopTatedServices.swift
          Videos/VideosServices.swift
          Search/SearchServices.swift
          Popular/PopularServices.swift
          Genres/GenresServices.swift

    presenter -> Es el intermediario entre el servicio y la vista, encargado de solicitar los datos pididos por la vista para ser solicitados por los servicios que luego seran enviados nuevamente  
                 la vista para su tratamiendo de una forma mas simple
          TopTated/TopTatedPresenter.swift
          Upcoming/UpcomingPresenter.swift
          Videos/VideosPresenter.swift
          Search/SearchPresenter.swift
          Popular/PopularPresenter.swift
          Configuration/ConfigurationPresenter.swift

    Resource -> aqui tenemos los Assets que usa la applicacion
          Assets.xcassets

    Settings -> aqui tenemos las extenciones, el storage, las constantes, info.plist, y el appDelegate  
        Storage/Storage.swift -> aqui guardamos en cache la informacion en cada consultas
        Extension/GradientView.swift
        Extension/extension.swift
        Constans/Constans.swift
        Info.plist
        AppDelegate.swift

    1. En qué consiste el principio de responsabilidad única? Cuál es su propósito?
        el principio de la responsabilidad única se basa en que cada modulo o clase daba tener una responsabilidad sobre una sola parte de la funcionalidad proporcionada, es decir que solo sea para una tarea o vista en espesifica y esta responsabilidad debe estar encapsulada en su totalidad por la clase.
    
    2. Qué características tiene, según su opinión, un “buen” código o código limpio 
      dado a mi experiencia como desarrollador pienso que las características mas resaltantes serian las siguientes:
         * Debe ser fácil de leer
         * Ser fácil de mantener
         * Ser flexible para mantenimientos o cambios
         * Parte importante del codigo debe estar comentada
         * Ser sencillo
         * Y lo mas importante que Funcione